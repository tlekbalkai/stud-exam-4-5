/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 11:46:00 by atlekbai          #+#    #+#             */
/*   Updated: 2018/04/06 12:11:16 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#define ABS(x) x < 0 ? -x : x

int		words(char *str)
{
	int i;
	int col;

	i = 1;
	col = 0;
	if (str[0] != ' ' && str[0] != '\t' && str[0] != '\n')
		col++;
	while (str[i])
	{
		if (str[i] != ' ' && str[i] != '\t' && str[i] != '\n' && (str[i - 1] == ' ' || str[i - 1] == '\n' || str[i - 1] == '\t'))
			col++;
		i++;
	}
	return (col);
}

int		letters(char *str)
{
	int i;

	i = 0;
	while (str[i] && str[i] != '\n' && str[i] != ' ' && str[i] != '\t')
		i++;
	return (i);
}

char **ft_split(char *str)
{
	char **res;
	int		i;
	int		j;
	int 	len;

	i = 0;
	len = words(str);
	res = (char **)malloc(sizeof(char *) * (len + 1));
	while (i < len)
	{
		while (*str == ' ' || *str == '\n' || *str == '\t')
			str++;
		j = 0;
		res[i] = (char *)malloc(sizeof(char) * (letters(str) + 1));
		while (*str && *str != ' ' && *str != '\t' && *str != '\n')
			res[i][j++] = *str++;
		res[i++][j] = '\0';
	}
	res[i] = 0;
	return (res);
}
