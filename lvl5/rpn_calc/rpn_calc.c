/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rpn_calc.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 12:12:28 by atlekbai          #+#    #+#             */
/*   Updated: 2018/04/06 12:21:55 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

t_list	*ft_new(int data)
{
	t_list *list;

	list = malloc(sizeof(t_list));
	if (list)
	{
		list->data = data;
		list->next = NULL;
	}
	return (list);
}

void	ft_list_add(t_list **list, int data)
{
	t_list *new;

	new = ft_new(data);
	new->next = *list;
	*list = new;
}

int		pop(t_list **list)
{
	t_list *tmp;
	int		data;

	tmp = *list;
	data = tmp->data;
	*list = tmp->next;
	free(tmp);
	return (data);
}

int	rpn_calc(char **res)
{
	int i;
	t_list *queue;

	i = 0;
	queue = (t_list*)malloc(sizeof(t_list));
	while (res[i])
	{
		if (res[i][0] >= 48 && res[i][0] <= '9')
			ft_list_add(&queue, atoi(res[i]));
		else if (res[i][0] == '+')
			ft_list_add(&queue, add(&queue));
		else if (res[i][0] == '-')
			ft_list_add(&queue, sub(&queue));
		else if (res[i][0] == '*')
			ft_list_add(&queue, mul(&queue));
		else if (res[i][0] == '/')
			ft_list_add(&queue, divide(&queue));
		else if (res[i][0] == '%')
			ft_list_add(&queue, modulo(&queue));
		i++;
	}
	if (queue->next->data == 0)
		return (queue->data);
	printf("Error");
	exit(0);
	return (0);
}
