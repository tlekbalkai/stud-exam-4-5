/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ops.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 12:23:25 by atlekbai          #+#    #+#             */
/*   Updated: 2018/04/06 12:25:28 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int	add(t_list **list)
{
	int a;
	int b;

	a = pop(list);
	b = pop(list);
	return (b + a);
}

int	sub(t_list **list)
{
	int a;
	int b;

	a = pop(list);
	b = pop(list);
	return (b - a);
}

int	mul(t_list **list)
{
	int a;
	int b;

	a = pop(list);
	b = pop(list);
	return (b * a);
}

int	divide(t_list **list)
{
	int a;
	int b;

	a = pop(list);
	b = pop(list);
	return (b / a);
}

int	modulo(t_list **list)
{
	int a;
	int b;

	a = pop(list);
	b = pop(list);
	return (b % a);
}
