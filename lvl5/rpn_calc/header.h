/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 11:42:55 by atlekbai          #+#    #+#             */
/*   Updated: 2018/04/06 11:59:28 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_H
# define HEADER_H

# include <stdlib.h>
# include <stdio.h>

typedef struct	s_list
{
	int				data;
	struct s_list	*next;
}				t_list;

char	**ft_split(char *str);
t_list	*ft_new(int data);
void	ft_list_add(t_list **list, int data);
int		pop(t_list **list);
int		rpn_calc(char **res);
int		add(t_list **list);
int		sub(t_list **list);
int		mul(t_list **list);
int		divide(t_list **list);
int		modulo(t_list **list);

#endif
