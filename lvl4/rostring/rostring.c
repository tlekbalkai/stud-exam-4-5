/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rostring.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 20:03:44 by atlekbai          #+#    #+#             */
/*   Updated: 2018/04/04 20:19:43 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

char	**ft_split(char *str);

int		elems(char **res)
{
	int i;

	i = 0;
	while (res[i])
		i++;
	return (i);
}

void	rostring(char *str)
{
	char **res;
	int i;
	char *temp;

	i = 1;
	res = ft_split(str);
	while (i < elems(res))
	{
		temp = res[i];
		res[i] = res[i - 1];
		res[i - 1] = temp;
		i++;
	}
	i = 0;
	while (res[i])
	{
		printf("%s", res[i]);
		if (i < elems(res) - 1)
			printf(" ");
		i++;
	}
}

int main(int ac, char **av)
{
	rostring(av[1]);
	return (0);
}
