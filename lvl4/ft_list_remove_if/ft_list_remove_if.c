/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_remove_if.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 21:17:53 by atlekbai          #+#    #+#             */
/*   Updated: 2018/04/04 21:27:57 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_list_remove_if(t_list **begin_list, void *data_ref, int (*cmp)())
{
	t_list *list;
	t_list *temp;

	list = *begin_list;
	while (list->next)
	{
		if (cmp(list->next->content, data_ref) == 0)
		{
			temp = list->next;
			list->next = temp->next;
			free(temp);
		}
		else
			list = list->next;
	}
	if (cmp((*begin_list)->content, data_ref) == 0)
	{
		temp = (*begin_list);
		*begin_list = temp->next;
		free(temp);
	}
}
