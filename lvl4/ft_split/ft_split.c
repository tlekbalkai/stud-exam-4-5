/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 19:36:52 by atlekbai          #+#    #+#             */
/*   Updated: 2018/04/04 19:59:38 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int		words(char *str)
{
	int i;
	int words;

	i = 1;
	words = 0;
	if (str[0] != ' ' && str[0] != '\n' && str[0] != '\t')
		words++;
	while (str[i])
	{
		if ((str[i] != '\n' && str[i] != '\t' && str[i] != ' ') && (str[i - 1] == '\n' || str[i - 1] == '\t' || str[i - 1] == ' '))
			words++;
		i++;
	}
	return (words);
}

int		letters(char *str)
{
	int i;

	i = 0;
	while (str[i] && str[i] != '\n' && str[i] != ' ' && str[i] != '\t')
		i++;
	return (i);
}


char	**ft_split(char *str)
{
	char **res;
	int i;
	int j;
	int len;

	len = words(str);
	res = (char **)malloc(sizeof(char *) * (len + 1));
	i = 0;
	while (i < len)
	{
		while (*str == '\n' || *str == '\t' || *str == ' ')
			str++;
		res[i] = (char *)malloc(sizeof(char) * (letters(str) + 1));
		j = 0;
		while (*str && *str != '\n' && *str != '\t' && *str != ' ')
			res[i][j++] = *str++;
		res[i++][j] = '\0';

	}
	res[i] = 0;
	return (res);
}
