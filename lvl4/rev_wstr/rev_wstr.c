/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rev_wstr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 18:46:16 by atlekbai          #+#    #+#             */
/*   Updated: 2018/04/04 19:20:30 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

void	rev_words(char *str)
{
	int i;
	int j;

	i = ft_strlen(str) - 1;
	while (i > -1)
	{
		while (str[i] != 32 && i >= 0 && str[i])
		{
			i--;
			j = i + 1;
		}
		while (str[j] && str[j] != 32)
			printf("%c", str[j++]);
		if (i > 0)
			printf(" ");
		i--;
	}
}

int main(int ac, char **av)
{
	rev_words(av[1]);
}
