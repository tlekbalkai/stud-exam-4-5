/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fprime.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 16:56:50 by atlekbai          #+#    #+#             */
/*   Updated: 2018/04/04 18:21:05 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int		is_prime(int c)
{
	int i;

	if (c < 2)
		return (0);
	if (c == 2)
		return (1);
	if (c % 2 == 0)
		return (0);
	i = 3;
	while (i < c)
	{
		if (c % i == 0)
			return (0);
		i += 2;
	}
	return (1);
}

void	prime_factors(int n)
{
	int i;

	i = 1;
	if (n == 1)
	{
		printf("1");
		return ;
	}
	while (n > 0 && i <= n)
	{
		if (is_prime(i))
			if (n % i == 0)
			{
				printf("%d", i);
				if (n / i != 1)
					printf("*");
				n = n / i;
				i = 0;
			}
		i++;
	}
}

int main(int ac, char **av)
{
	prime_factors(atoi(av[1]));
}
