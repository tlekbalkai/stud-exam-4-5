/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atlekbai <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 18:23:53 by atlekbai          #+#    #+#             */
/*   Updated: 2018/04/04 18:44:31 by atlekbai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#define ABS(x) x < 0 ? -x : x

int		num_len(int n)
{
	int i;

	i = 0;
	while (n > 0)
	{
		i++;
		n /= 10;
	}
	return (i);
}

char	*ft_itoa(int nbr)
{
	char *res;
	int nbr_len;
	int nbr_cpy;

	nbr_len = num_len(ABS(nbr));
	nbr_cpy = nbr;
	if (nbr < 0)
	{
		nbr_cpy = -nbr;
		nbr_len++;
	}
	res = (char *)malloc(sizeof(char) * (nbr_len + 1));
	if (!res)
		return (0);
	printf("%d\n", nbr_len);
	res[nbr_len] = '\0';
	res[--nbr_len] = nbr_cpy % 10 + 48;
	while (nbr_cpy /= 10)
		res[--nbr_len] = nbr_cpy % 10 + 48;
	if (nbr < 0)
		res[0] = '-';
	return (res);
}

int		main(int ac, char **av)
{
	printf("%s\n", ft_itoa(atoi(av[1])));
}
